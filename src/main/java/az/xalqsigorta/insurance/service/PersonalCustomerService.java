package az.xalqsigorta.insurance.service;

import az.xalqsigorta.insurance.mapper.PersonalCustomerMapper;
import az.xalqsigorta.insurance.mapper.PersonalCustomerMapperImpl;
import az.xalqsigorta.insurance.repository.PersonalCustomerRepo;
import az.xalqsigorta.insurance.dto.PersonalCustomerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Service
public class PersonalCustomerService {

    private  final PersonalCustomerRepo personalCustomerRepo;
    private  final PersonalCustomerMapper mapper;

    public PersonalCustomerService(PersonalCustomerRepo personalCustomerRepo, PersonalCustomerMapperImpl mapper) {
        this.personalCustomerRepo = personalCustomerRepo;
        this.mapper = mapper;
    }


    public List<PersonalCustomerDto> getAll() {

        return mapper.mapToDto(personalCustomerRepo.findAll());
    }

}

