package az.xalqsigorta.insurance.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.boot.autoconfigure.web.WebProperties;
@Data
@Entity
@Table(name = "personal_customers")

public class PersonalCustomer {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String first_name;
    private String last_name;
    private String nationality_code;



}
