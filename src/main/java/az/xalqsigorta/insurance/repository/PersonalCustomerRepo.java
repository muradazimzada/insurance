package az.xalqsigorta.insurance.repository;

import az.xalqsigorta.insurance.entity.PersonalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PersonalCustomerRepo extends JpaRepository<PersonalCustomer, Long> {
}
