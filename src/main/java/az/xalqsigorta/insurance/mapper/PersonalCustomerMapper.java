package az.xalqsigorta.insurance.mapper;

import az.xalqsigorta.insurance.dto.PersonalCustomerDto;
import az.xalqsigorta.insurance.entity.PersonalCustomer;
import org.mapstruct.BeanMapping;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.context.annotation.Bean;

import java.util.List;
@Mapper(componentModel = "spring",injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PersonalCustomerMapper {

    @Mapping(target = "firstName", source = "entity.first_name")
    @Mapping(target = "lastName", source = "entity.last_name")
    @Mapping(target = "nationalityCode", source = "entity.nationality_code")
    PersonalCustomerDto mapToDto(PersonalCustomer entity);


    @Mapping(target = "first_name", source = "dto.firstName")
    @Mapping(target = "last_name", source = "dto.lastName")
    @Mapping(target = "nationality_code", source = "dto.nationalityCode")
    PersonalCustomer mapToEntity(PersonalCustomerDto dto);

    List<PersonalCustomerDto> mapToDto (List<PersonalCustomer> personalCustomers);
}
