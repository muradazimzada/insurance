package az.xalqsigorta.insurance.controller;

import az.xalqsigorta.insurance.dto.PersonalCustomerDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import az.xalqsigorta.insurance.service.PersonalCustomerService;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/v1/personal-customers/")
public class PersonalCustomerController {

    private final PersonalCustomerService service;

    public PersonalCustomerController(PersonalCustomerService service) {
        this.service = service;
    }

    @GetMapping()
    public List<PersonalCustomerDto> getAll() {

        return  service.getAll();
    }
}
